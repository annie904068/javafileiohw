package com.tgl.fileio.hw;

import java.util.List;

public class Search {

	public void searchAll (List<Employee> list, String option, String input) {
		try{
			switch(Condition.valueOf(option)) {
			case id : 
				System.out.println(searchId(list, Integer.valueOf(input)));
				break;
			case englishname:
				System.out.println(searchEnglishname(list, input));
				break;
			case chinesename:
				System.out.println(searchChinesename(list, input));
				break;
			case phone:
				System.out.println(searchPhone(list, input));
				break;
			case email:
				System.out.println(searchEmail(list, input));
				break;
			default:
				break;
			}
		}catch(IllegalArgumentException e){
			System.out.println("Search條件選擇如下，請在試一次");
			for(Condition search: Condition.values()) {
				if(search.toString() != "height" && search.toString() != "weight" && search.toString() != "bmi")
					System.out.print(search + " ");
			}
		}
	}

	public String searchId(List<Employee> list, int s) {
		for (Employee element : list) {
			if (s == element.getRollId()) {
				return element.toString();
			}
		}
		return "Error, try again, please.";
	}

	public String searchEnglishname(List<Employee> list, String s) {
		for (Employee element : list) {
			if (s.compareTo(element.getEnglishName()) == 0) {
				return element.toString();
			}
		}
		return "Error, try again, please.";
	}

	public String searchChinesename(List<Employee> list, String s) {
		for (Employee element : list) {
			if (s.compareTo(element.getChineseName()) == 0) {
				return element.toString();
			}
		}
		return "Error, try again, please.";
	}

	public String searchPhone(List<Employee> list, String s) {
		for (Employee element : list) {
			if (s.compareTo(element.getPhone()) == 0) {
				return element.toString();
			}
		}
		return "Error, try again, please.";
	}

	public String searchEmail(List<Employee> list, String s) {
		for (Employee element : list) {
			if (s.compareTo(element.getEmail()) == 0) {
				return element.toString();
			}
		}
		return "Error, try again, please.";
	}
}
