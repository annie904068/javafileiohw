package com.tgl.fileio.hw;

import java.util.List;

public class Delete {
	
	public void deleteAll (List<Employee> list, String option, String input) {
		try{
			switch(Condition.valueOf(option)) {
			case id : 
				System.out.println(deleteRollId(list, Integer.valueOf(input)));
				break;
			case englishname:
				System.out.println(deleteEnglishname(list, input));
				break;
			case phone:
				System.out.println(deletePhone(list, input));
				break;
			default:
				break;
			}
		}catch(IllegalArgumentException e){
			System.out.println("Delete條件選擇如下，請在試一次");
			for(Condition search: Condition.values()) {
				if(search.toString() == "id" || search.toString() == "englishname" || search.toString() == "phone")
					System.out.print(search + " ");
			}
		}
	}
	
	public String deleteRollId(List<Employee> list, int id) {
		for(int i = 0; i < list.size(); i++) {
			if(id == list.get(i).getRollId()) {
				list.remove(i);
				return Integer.toString(list.size());
			}
		}
		return "Error, try again, please.";
	}
	
	public String deleteEnglishname(List<Employee> list, String s) {
		for(int i = 0; i < list.size(); i++) {
			if(s.compareTo(list.get(i).getEnglishName()) == 0) {
				list.remove(i);
				return Integer.toString(list.size());
			}
		}
		return "Error, try again, please.";
	}
	
	public String deletePhone(List<Employee> list, String s) {
		for(int i = 0; i < list.size(); i++) {
			if(s.compareTo(list.get(i).getPhone()) == 0) {
				list.remove(i);
				return String.valueOf(list.size());
			}
		}
		return "Error, try again, please.";
	}
}
