package com.tgl.fileio.hw;

import java.util.Collections;
import java.util.List;

public class Maxmin {
	public void maxminAll (List<Employee> list, String option) {
		try{
			switch(Condition.valueOf(option)) {
			case height : 
				System.out.println(Maxminheight(list));
				break;
			case weight:
				System.out.println(Maxminweight(list));
				break;
			case bmi:
				System.out.println(Maxminbmi(list));
				break;
			default:
				break;
			}
		}catch(IllegalArgumentException e){
			System.out.println("Maxmin條件選擇如下，請在試一次");
			for(Condition search: Condition.values()) {
				if(search.toString() == "height" || search.toString() == "weight" || search.toString() == "bmi")
					System.out.print(search + " ");
			}
		}
	}
	
	public String Maxminheight(List<Employee> list) {
		Collections.sort(list, new SortHeight());
		String result = "Min: " + list.get(0) + "\n" + "Max: " + list.get(list.size() - 1);
		return result;
	}
	
	public String Maxminweight(List<Employee> list) {
		Collections.sort(list, new SortWeight());
		String result = "Min: " + list.get(0) + "\n" + "Max: " + list.get(list.size() - 1);
		return result;
	}
	
	public String Maxminbmi(List<Employee> list) {
		Collections.sort(list, new SortBmi());
		String result = "Min: " + list.get(0) + "\n" + "Max: " + list.get(list.size() - 1);
		return result;
	}
}
