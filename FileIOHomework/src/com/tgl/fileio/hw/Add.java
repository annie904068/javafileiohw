package com.tgl.fileio.hw;

import java.util.List;

public class Add {
	public String addAll(List<Employee> list, String input) {
		String regex = "[\\u4e00-\\u9FA5]+";
		try {
		String[] c = input.split(" ");
		Integer.valueOf(c[0]);
		Integer.valueOf(c[1]);
		Integer.valueOf(c[4]);
		if(c.length != 6) {
			return "Input error.";
		}else if(!(c[2].contains("-"))){
			return "English name error.";
		}else if(!(c[3].matches(regex))) {
			System.out.println(c[3]);
			return "Chinese name error.";
		}else if(!(c[5].contains("@"))) {
			return "Email error.";
		}else{
			int id = list.size() + 1;
			list.add(new Employee(id, c[0], c[1], c[2], c[3], c[4], c[5], FileIOHomework.BMI(c[0], c[1])));
			return Integer.toString(list.size());
		}
		}catch(IllegalArgumentException e) {
			return "Add input error";
		}
	}
}
