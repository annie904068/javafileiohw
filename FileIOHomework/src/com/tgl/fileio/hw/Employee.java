package com.tgl.fileio.hw;

public class Employee {
	
	private int rollId;
	private String height;
	private String weight;
	private String englishName;
	private String chineseName;
	private String phone;
	private String email;
	private double bmi;
	
	public Employee(int rollId, String height, String weight, String englishName, String chineseName, String phone, String email, double bmi) {
		this.rollId = rollId;
		this.height = height;
		this.weight = weight;
		this.englishName = englishName;
		this.chineseName = chineseName;
		this.phone = phone;
		this.email = email;
		this.bmi = bmi;
	}

	public int getRollId() {
		return rollId;
	}
	public void setRollId(int rollId) {
		this.rollId = rollId;
	}

	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}

	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	
	public String getEnglishName() {
		return englishName;
	}
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	
	public String getChineseName() {
		return chineseName;
	}
	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public double getBmi() {
		return bmi;
	}
	public void setBmi(double bmi) {
		this.bmi = bmi;
	}

	@Override
	public String toString() {
		return "Employee[id:" + rollId + " height:" + height + " weight:" + weight +" english name:" + englishName + " chinese name:" + chineseName + " phone:" + phone + " email:" + email + " BMI:" + bmi + "]";
	}
}
