package com.tgl.fileio.hw;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class FileIOHomework {
	public static void main(String[] args) {
		List<Employee> list = new ArrayList<>();
		String line;
		String[] word;
		int i = 1;
		double bmi;

		try (BufferedReader file = new BufferedReader(new FileReader("D:\\fileiodata.txt"))) {
			while ((line = file.readLine()) != null) {
				word = line.split(" ");
				bmi = BMI(word[0], word[1]);
				list.add(new Employee(i, word[0], word[1], word[2], word[3], word[4], word[5], bmi));
				i++;
			}
			// file.close();
		} catch (Exception e) {
			System.out.println(e);
		}

		String rollId = "18";
		String height = "187";
		String weight = "78";
		String englishname = "Jet-Yang";
		String chinesename = "劉聖緯";
		String phone = "6997";
		String email = "jeremycheng@transglobe.com.tw";
		String condition = height + " " + weight + " " + englishname + " " + chinesename + " " + phone + " " + email;
		
		Functions f = new Functions();		
		f.methodAll(list, Method.add, condition, "");
	}

	public static double BMI(String height, String weight) {
		float h = Float.parseFloat(height);
		float w = Float.parseFloat(weight);
		return w / (h * 0.01 * h * 0.01);
	}
}