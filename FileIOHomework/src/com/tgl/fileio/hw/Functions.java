package com.tgl.fileio.hw;

import java.util.List;

public class Functions {

	public void methodAll(List<Employee> list, Method method, String inputcondition, String inputdata) {
		Search search = new Search();
		Add add = new Add();
		Delete delete = new Delete();
		Maxmin maxmin = new Maxmin();
		try {
			switch (method) {
			case search:
				search.searchAll(list, inputcondition, inputdata);
				break;
			case sort:
				Router.sortAll(list, inputcondition);
				break;
			case add:
				System.out.println(add.addAll(list, inputcondition));
				break;
			case delete:
				delete.deleteAll(list, inputcondition, inputdata);
				break;
			case maxmin:
				maxmin.maxminAll(list, inputcondition);
				break;
			default:
				break;
			}
		} catch (IllegalArgumentException e) {
			System.out.println("CRUD選擇如下，請在試一次");
			for (Method m : Method.values()) {
				System.out.print(m + " ");
			}
		}
	}
}
