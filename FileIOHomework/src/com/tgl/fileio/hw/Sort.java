package com.tgl.fileio.hw;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Router {
	public static void sortAll(List<Employee> list, String option) {
		try {
			switch (Condition.valueOf(option)) {
			case id:
				Collections.sort(list, new SortId());
				for (Employee employee : list) {
					System.out.println(employee);
				}
				break;
			case height:
				Collections.sort(list, new SortHeight());
				for (Employee employee : list) {
					System.out.println(employee);
				}
				break;
			case weight:
				Collections.sort(list, new SortWeight());
				for (Employee employee : list) {
					System.out.println(employee);
				}
				break;
			case englishname:
				Collections.sort(list, new SortEnglishName());
				for (Employee employee : list) {
					System.out.println(employee);
				}
				break;
			case phone:
				Collections.sort(list, new SortPhone());
				for (Employee employee : list) {
					System.out.println(employee);
				}
				break;
			case email:
				Collections.sort(list, new SortEmail());
				for (Employee employee : list) {
					System.out.println(employee);
				}
				break;
			case bmi:
				Collections.sort(list, new SortBmi());
				for (Employee employee : list) {
					System.out.println(employee);
				}
				break;
			default:
				break;
			}
		} catch (IllegalArgumentException e) {
			System.out.println("Sort條件選擇如下，請在試一次");
			for (Condition search : Condition.values()) {
				if (search.toString() != "chinesename")
					System.out.print(search + " ");
			}
		}
	}
}

class SortId implements Comparator<Employee> {
	@Override
	public int compare(Employee e1, Employee e2) {
		if (e1.getRollId() > e2.getRollId())
			return 1;
		else if (e1.getRollId() < e2.getRollId())
			return -1;
		else
			return 0;
	}
}

class SortHeight implements Comparator<Employee> {
	@Override
	public int compare(Employee e1, Employee e2) {
		int h1 = Integer.parseInt(e1.getHeight().toString());
		int h2 = Integer.parseInt(e2.getHeight().toString());
		if (h1 > h2)
			return 1;
		else if (h1 < h2)
			return -1;
		else
			return 0;
	}
}

class SortWeight implements Comparator<Employee> {
	@Override
	public int compare(Employee e1, Employee e2) {
		int w1 = Integer.parseInt(e1.getWeight().toString());
		int w2 = Integer.parseInt(e2.getWeight().toString());
		if (w1 > w2)
			return 1;
		else if (w1 < w2)
			return -1;
		else
			return 0;
	}
}

class SortEnglishName implements Comparator<Employee> {
	@Override
	public int compare(Employee e1, Employee e2) {
		if (e1.getEnglishName().compareTo(e2.getEnglishName()) > 0)
			return 1;
		else if (e1.getEnglishName().compareTo(e2.getEnglishName()) < 0)
			return -1;
		else
			return 0;
	}
}

class SortPhone implements Comparator<Employee> {
	@Override
	public int compare(Employee e1, Employee e2) {
		if (e1.getPhone().compareTo(e2.getPhone()) > 0)
			return 1;
		else if (e1.getPhone().compareTo(e2.getPhone()) < 0)
			return -1;
		else
			return 0;
	}
}

class SortEmail implements Comparator<Employee> {
	@Override
	public int compare(Employee e1, Employee e2) {
		if (e1.getEmail().compareTo(e2.getEmail()) > 0)
			return 1;
		else if (e1.getEmail().compareTo(e2.getEmail()) < 0)
			return -1;
		else
			return 0;
	}
}

class SortBmi implements Comparator<Employee> {
	@Override
	public int compare(Employee e1, Employee e2) {
		if (e1.getBmi() > e2.getBmi())
			return 1;
		else if (e1.getBmi() < e2.getBmi())
			return -1;
		else
			return 0;
	}
}
